import tornado
import tornado.web, tornado.httpserver

LISTENING_PORT=8191
SITE_DIR = '/var/www/model-viewer-test/'

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index-threejs.html")

def make_app():
    settings = {
        "xsrf_cookies": True,
	    "cookie_secret" : "dumbdumdomedum",
        "login_url":"/login",
    }
    return tornado.web.Application([
        (r"/", MainHandler),
        (r'/file/(.*)', tornado.web.StaticFileHandler, {'path': SITE_DIR + "/files/"}),
        (r'/build/(.*)', tornado.web.StaticFileHandler, {'path': SITE_DIR + "/files/"}),
        (r'/loaders/(.*)', tornado.web.StaticFileHandler, {'path': SITE_DIR + "/files/"}),
        
    ], debug=True, **settings)



if __name__ == "__main__":
    http_server = tornado.httpserver.HTTPServer(
        make_app(),
    )
    http_server.listen(LISTENING_PORT) 
    tornado.ioloop.IOLoop.current().start()
