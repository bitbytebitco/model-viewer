# Description 

In undergraduate I spent many hours using 3D modeling programs like Google Sketchup. Recently I decided to see about bridging my interest in 3D with my programming ability, which led me to build this simple three.js application.

## Demonstration 

View this live at [https://threejs.diluo.org](https://threejs.diluo.org)
